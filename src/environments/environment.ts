// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
// import { initializeApp } from "firebase/app";
// import { getAnalytics } from "firebase/analytics";


export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCCVj9xdVK9xQ6MTBfup2J5iZU8Kd4hThg",
    authDomain: "desafio-7-dfd22.firebaseapp.com",
    projectId: "desafio-7-dfd22",
    storageBucket: "desafio-7-dfd22.appspot.com",
    messagingSenderId: "756544990741",
    appId: "1:756544990741:web:e44e5c423d86ffa992a81c",
    measurementId: "G-VW1RB2Z6GR"
  }
};

// const app = initializeApp(environment.firebaseConfig);
// const analytics = getAnalytics(app);

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
