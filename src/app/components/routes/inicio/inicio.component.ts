import { Component, OnInit } from '@angular/core';
import { Movie_Series, Result, Trending } from 'src/Interfaces/Movie_Series';
import { MoviesService } from 'src/app/Services/movies.service';
import { Series } from 'src/Interfaces/Series';
import { Movie } from 'src/Interfaces/Movie';
import { MovieSerieBase } from 'src/Interfaces/MovieSerieBase';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],
  providers: [MoviesService]
})
export class InicioComponent implements OnInit {
  
  public filter: number;

  
  
  
  

  movie_series: Result[] = [
   
  ]
  
  moviesAux: Result[] = [];
  moviesBuscar: Result[] = [];

  constructor(
    private moviesServices: MoviesService,
    
  ) {
    this.filter = 1;
    

   }

  ngOnInit(): void {
    this.getTrending();
    this.getMovies();
    this.getSeries();
  }

  getTrending() {
    this.moviesServices.getTrending().subscribe({
      next: (data: Trending) => {
        console.log(data.results);
        this.movie_series = data.results
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La petición terminó');
      }
    })
  }

  getMovies() {
    this.moviesServices.getMovies().subscribe({
      next: (data: Trending) => {
        console.log(data.results);
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La petición terminó');
      }
    })
  }

  getSeries() {
    this.moviesServices.getSeries().subscribe({
      next: (data: Trending) => {
        console.log(data.results);
      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La petición terminó');
      }
    })
  }

  onPrimerBoton(tipo : string){
    this.filter = 1;
    if(tipo == 'Todos' ) {
      this.moviesAux = this.movie_series
    }
  }
  onSegundoBoton(tipo : string){
    this.filter = 2;
    if(tipo == 'movie') {
      this.moviesAux = this.movie_series.filter(tipo => tipo.media_type == 'movie')
    }
  }
  onTercerBoton(tipo : string){
    this.filter = 3;
    if(tipo == 'tv') {
      this.moviesAux = this.movie_series.filter(tipo => tipo.media_type == 'tv')
    }
  }

 


search: string = ''

tosearch(){
  this.moviesBuscar = [];
  this.moviesAux = this.movie_series
  for (let prog of this.moviesAux) {
    if (
      prog.media_type == 'movie' &&
      prog.title.toUpperCase().includes(this.search.toUpperCase())
    ) {
      this.moviesBuscar.push(prog);
    }
    if(
      prog.media_type == 'tv'&&
      prog.title.toUpperCase().includes(this.search.toUpperCase())
    ) {
      this.moviesBuscar.push(prog);
    }
  }
  if(this.search !== ''){
    this.moviesAux = this.moviesBuscar;
  }else{
    this.moviesAux = this.movie_series;
  }
}

addList(item: MovieSerieBase){
  const idUser = JSON.parse(localStorage.getItem('usuario')!).uid
  this.moviesServices.addItem(idUser, item)

}

// deleteItem(id:string) {
    
//     this._movieService.deleteItem(this.user.uid, id).then( () => {
//     console.log('se elimino correctamente')
//     }).catch( error => {
//     console.log(error)
//     })
//     }





}

// series: Pelisplus[] = [];
// movie: Pelisplus[] = [];
// cont = 0;

// seccion(id: number){
//   if(id!=1)
//   {
//     if(id==2){
//       for(let i = 0; i < this.movie_series.length; i++){
//         if(this.movie_series[i].category=='pelicula'){
//           this.movie[this.cont]=this.movie_series[i];
//           this.tipo=id;
//           this.cont++;
//         }
//       }
//     }else if(id==3)
//     {
//       for(let i = 0; i < this.movie_series.length; i++){
//         if(this.movie_series[i].category=='series'){
//           this.series[this.cont]=this.movie_series[i];
//           this.tipo=id;
//           this.cont++;
//         }
//       }
//     }
//   }else this.tipo=1
//   this.cont=0;
//   return
// }

// setSeccion(){
//   return this.tipo;
// }