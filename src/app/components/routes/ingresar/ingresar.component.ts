import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFireAuth, AngularFireAuthModule } from '@angular/fire/compat/auth';
import { AuthServiceService} from '../../../Services/auth-service.service'



import  firebase  from 'firebase/compat/app'
import { Router, Routes } from '@angular/router';
// import firebase from 'firebase';

@Component({
  selector: 'app-ingresar',
  templateUrl: './ingresar.component.html',
  styleUrls: ['./ingresar.component.css']
})
export class IngresarComponent implements OnInit {
  miFormulario: FormGroup = this.fb.group({
    email: [, [Validators.required, Validators.email, Validators.minLength(4)]],
    contraseña: [, [ Validators.required, Validators.minLength(6)]],

    
  });

  constructor(private fb: FormBuilder, private auth: AuthServiceService, private routes:Router ) {}

  ngOnInit(): void {
    this.miFormulario.reset({
      email: 'alejandrosg197@gmail.com',
      contraseña: 12244896,
    });
  }

  campoEsValido(campo: string) {
    return(
      this.miFormulario.controls[campo].errors &&
      this.miFormulario.controls[campo].touched
    );
  }

  guardar() {
    if (this.miFormulario.invalid) {
      this.miFormulario.markAllAsTouched();
      return;
    }
    if (this.miFormulario.valid)
    this.Ingresar(this.miFormulario.value)

  }



Ingresar(form:any) { 
    
    const { email, password } = form.controls;
    this.auth.login(email.value, password.value).then(res => {
      if (!res)
      return
        console.log("Se Registro: ", res);
        this.routes.navigate(['/inicio'])
    })
}

IngresarConGoogle() { 
  console.log("Ingresar");
  
    this.auth.loginWithGoogle().then(res => {
      if (!res)
      return
      console.log(res)
      localStorage.setItem('usuario', JSON.stringify(res.user))
        console.log("Se Registro: ", res);
        this.routes.navigate(['/inicio'])
    })
}



}
