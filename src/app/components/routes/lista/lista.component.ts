import { Component, OnInit } from '@angular/core';
import { Movie_Series, Result } from 'src/Interfaces/Movie_Series';
import { MoviesService } from 'src/app/Services/movies.service';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  public filter: string = 'Todos';
  user: any;
  movies: Result[] = [];

  constructor(
    private _movieService: MoviesService
  ) { }

  ngOnInit(): void {
    const usuario = localStorage.getItem('usuario')
    if (usuario) this.user =JSON.parse(usuario);
      
      this.getMovies();
  }

  getMovies() {
      this._movieService.getList(this.user.uid).subscribe(
      response => {
      this.movies = [];
      console.log("esto es un response",response)
      response.forEach((element:any) => {
      // console.log(element.patload.doc.id)
      // console.log(element.payload.doc.data())
      this.movies.push({
      idGlobal: element.payload.doc.id,
      ...element.payload.doc.data(),
    })
    })
    console.log("esta es la peticion de movies", this.movies)
    },
    error => {
    console.log("fallo la peticion de movies", error)
    }
    )
    }
  
    deleteItem(id:string) {
    
    this._movieService.deleteItem(this.user.uid, id).then( () => {
    console.log('se elimino correctamente')
    }).catch( error => {
    console.log(error)
    })
    }


}



  
  
//   ngOnInit(): void {
//   this.user =JSON.parse(localStorage.getItem('usuario'));
//   this.getMovies();
//   }
  
//   getMovies() {
//   this._movieService.gerList(this.user.uid).subscribe(
//   response => {
//   this.movies = [];
//   console.log("esto es un response",response)
//   response.forEach((element:any) => {
//   // console.log(element.patload.doc.id)
//   // console.log(element.payload.doc.data())
//   this.movies.push({
//   idGlobal: element.payload.doc.id,
//   ...element.payload.doc.data(),
//   })
//   })
//   console.log("esta es la peticion de movies", this.movies)
//   },
//   error => {
//   console.log("fallo la peticion de movies", error)
//   }
//   )
//   }
  
//   deleteItem(id:string) {
//   this.user = JSON.parse(localStorage.getItem('usuario'));
//   this.-movieService.deleteItem(this.user.uid, id).then( () => {
//   console.log('se elimino correctamente')
//   }).catch( error => {
//   console.log(error)
//   })
//   }
  
//   }