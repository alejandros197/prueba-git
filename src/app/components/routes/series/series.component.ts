import { Component, OnInit } from '@angular/core';
import { Movie_Series, Result, Trending } from 'src/Interfaces/Movie_Series';
import { MoviesService } from 'src/app/Services/movies.service';
import { MovieSerieBase } from 'src/Interfaces/MovieSerieBase';

@Component({
  selector: 'app-series',
  templateUrl: './series.component.html',
  styleUrls: ['./series.component.css']
})
export class SeriesComponent implements OnInit {

  movie_series: Result[] = [
   
  ]
  moviesAux: Result[] = [];

  constructor(private moviesServices: MoviesService,) { }

  ngOnInit(): void {
    this.getSeries()
  }

  getSeries() {
    this.moviesServices.getSeries().subscribe({
      next: (data: Trending) => {
        console.log(data.results);
        this.moviesAux = data.results

      },
      error: (err) => {
        console.log(err);
      },
      complete: () => {
        console.log('La petición terminó');
      }
    })
  

  }

  addList(item: MovieSerieBase){
    const idUser = JSON.parse(localStorage.getItem('usuario')!).uid
    this.moviesServices.addItem(idUser, item)
  
  }

}
