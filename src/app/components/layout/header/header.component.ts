import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthServiceService } from 'src/app/Services/auth-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public userLogged: Observable<any> = this.authService.afauth.user;
  

  constructor(private authService: AuthServiceService) { }

  ngOnInit(): void {
  }

  public logOut(){
this.authService.logout();
}

}
