import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 

  constructor() {
    
    this.grabar_localstorage();
    
    this.obtener_localstorage();
    
    this.prueba_localstorage();

    this.removeLocalStorage();
  }

  obtener_localstorage(){
    
    let nombre = localStorage.getItem("nombre");
    // let persona =  JSON.parse(localStorage.getItem("persona")) || null;
    let persona = localStorage.getItem("persona");


    console.log(nombre);
    console.log(persona);

  }

  grabar_localstorage(){

    let nombre:string = "Alejandro";

    let persona = {
      nombre:"Juan",
      edad: 24,
      datos: {
        email: "juanmail@gmail.com",
        direc: "Calle falsa 123 (O)"
      }
    }

    localStorage.setItem("nombre", nombre);
    localStorage.setItem("persona", JSON.stringify(persona));



  }

  prueba_localstorage() {
    let prueba:string = "Prueba-LocalStorage";
    localStorage.setItem("prueba",prueba);
  }

  removeLocalStorage() {
    localStorage.removeItem('prueba');
  }


}