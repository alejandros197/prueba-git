import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Trending } from 'src/Interfaces/Movie_Series';
import { Movie } from 'src/Interfaces/Movie';
import { Series } from 'src/Interfaces/Series';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { promises } from 'dns';
import { MovieSerieBase } from 'src/Interfaces/MovieSerieBase';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  private app_id: string = 'f42e2de612be89f03e8768e82829a9d6';
  private baseUrl: string = 'https://api.themoviedb.org/3';

  constructor(
    private _http : HttpClient, private firestore: AngularFirestore
  ) { }

  addUser(userId: string): Promise <any> {
    return this.firestore.collection('usuarios').add(userId)
  }

  addItem(userId: string, item: MovieSerieBase): Promise <any> {
    return this.firestore.collection('usuarios').doc(`${userId}`).collection('movies').add(item)
  }

  getList(userId: MovieSerieBase): Observable<any> {
    return this.firestore.collection('usuarios').doc(`${userId}`).collection('movies').snapshotChanges()
  }

  deleteItem(idUser:string, id: string): Promise<any> {
    return this.firestore.collection(`usuarios/${idUser}/movies`).doc(id).delete();
  }
  

  getTrending() : Observable<Trending> {
    let params = new HttpParams().set('api_key', this.app_id);

    return this._http.get<Trending>(this.baseUrl + '/trending/all/week', {
      params : params
    })
  }

  getMovies() : Observable<Trending> {
    let params = new HttpParams().set('api_key', this.app_id);

    return this._http.get<Trending>(this.baseUrl + '/movie/popular', {
      params : params
    })
  }

  getSeries() : Observable<Trending> {
    let params = new HttpParams().set('api_key', this.app_id);

    return this._http.get<Trending>(this.baseUrl + '/tv/popular', {
      params : params
    })
  }


}
