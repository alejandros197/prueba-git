import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { async } from '@firebase/util';
import  firebase  from 'firebase/compat/app'

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(private afauth: AngularFireAuth) { }
}
// async login(email: 'string', password: 'string') {
//     try{
//         return await this.afauth.signInWithEmailAndPassword(email, password);
//     }   catch (err) {
//         console.log("error en login: ", err);
//         return null;
//     }
// }
// async loginWithGoogle(email: 'string', password: 'string') {
//     try{
//         return await this.afauth.singInWithPopup(new firebase.auth.GoogleAuthProvider());
//     }   catch (err) {
//         console.log("error en login con google: ", err);
//         return null;
//     }
// }

// -----------------------COPIA UNO-----------------------

// async login(email: 'string', password: 'string') {
//     try{
//         return await this.afauth.signInWithEmailAndPassword(email, password);
//     }   catch (err) {
//             console.log("error en login: ", err);
//             return null;
//         }
    
// }

// async loginWithGoogle(email: 'string', password: 'string') {
//     try{
//         return await this.afauth.singInWithPopup(new firebase.auth.GoogleAuthProvider());
//     }   catch   (err) {
//         console.log("error en login con google: ", err);
//         return null;
//     }
// }

// Ingresar() { 
//     console.log(this.usuario);
//     const { email, password } = this.usuario;
//     this.AuthService.register(email, password).then(res => {
//         console.log(res)
//     })
// }

// IngresarConGoogle() { 
//     const { email, password } = this.usuario;
//     this.AuthService.loginWithGoogle(email, password).then(res => {
//         console.log("se registro: ", res);
//     })
// }

// getUserLogged() {
//     return this.afauth.authState;
// }
// logout() { 
//     this.afauth.singOut();
// }
