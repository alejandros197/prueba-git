export interface Movie_Series {
    id:             number;
    name:           string;
    description:    string;
    image:          string;
    rating:         number;
    category:       string;
    category2:      string;
}

export interface Trending {
    page:          number;
    results:       Result[];
    total_pages:   number;
    total_results: number;
}

export interface Result {
    video?:            boolean;
    vote_average:      number;
    overview:          string;
    release_date?:     Date;
    id:                number;
    adult?:            boolean;
    backdrop_path:     string;
    vote_count:        number;
    genre_ids:         number[];
    title:            string;
    original_language: OriginalLanguage;
    original_title?:   string;
    poster_path:       string;
    popularity:        number;
    media_type:        MediaType;
    original_name?:    string;
    origin_country?:   string[];
    first_air_date?:   Date;
    name?:             string;
    idGlobal?:          string;
}

export enum MediaType {
    Movie = "movie",
    Tv = "tv",
}

export enum OriginalLanguage {
    En = "en",
    Ja = "ja",
}
